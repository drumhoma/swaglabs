from .base_selectors import xpath


class LoginPageLocators:

    LOGIN_LOGO = (xpath, "//div[@class='login_logo']")

    USERNAME_FIELD = (xpath, "//input[@id='user-name']")
    PASSWORD_FIELD = (xpath, "//input[@id='password']")
    LOGIN_BUTTON = (xpath, "//input[@id='login-button']")

    ERROR_MESSAGE = (xpath, "//div[@class='error-message-container error']")
