from .login_page import LoginPageLocators
from .inventory_page import InventoryPageLocators
from .cart_page import CartPageLocators
from .checkout_page import CheckOutPageLocators
from .main_page import MainPageLocators
