from .base_selectors import xpath


class InventoryPageLocators:

    SELECT_MENU = (xpath, "//select[@class='product_sort_container']")
    SELECT_VALUE = (xpath, "//option[@value]")

    ADD_TO_CART = (xpath, "//button[@class='btn btn_primary btn_small btn_inventory']")  # найти все 6
    REMOVE_FROM_CART = (xpath, "//button[@class='btn btn_secondary btn_small btn_inventory']")  # найти все 6
    NUM_IN_CART = (xpath, "//span[@class='shopping_cart_badge']")

    ITEM_NAME_IN_LINK = (xpath, "//div[@class='inventory_item_name']")
    ITEM_PRICE_IN_LINK = (xpath, "//div[@class='inventory_item_price']")
    ITEM_NAME_IN_CARD = (xpath, "//div[@class='inventory_details_name large_size']")
    ITEM_PRICE_IN_CARD = (xpath, "//div[@class='inventory_details_price']")

    BACK_TO_INVENTORY = (xpath, "//button[@id='back-to-products']")
