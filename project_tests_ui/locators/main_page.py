from .base_selectors import xpath


class MainPageLocators:

    APP_LOGO = (xpath, "//div[@class='app_logo']")

    SIDE_MENU = (xpath, "//button[@id='react-burger-menu-btn']")
    CLOSE_MENU = (xpath, "//button[@id='react-burger-cross-btn']")

    SHOP_CART = (xpath, "//a[@class='shopping_cart_link']")

    TWITTER_LINK = (xpath, "//li[@class='social_twitter']")
    FB_LINK = (xpath, "//li[@class='social_facebook']")
    LINKEDIN_LINK = (xpath, "//li[@class='social_linkedin']")