from .base_selectors import xpath


class CheckOutPageLocators:

    FIRST_NAME = (xpath, "//input[@id='first-name']")
    LAST_NAME = (xpath, "//input[@id='last-name']")
    ZIP_CODE = (xpath, "//input[@id='postal-code']")

    CONTINUE_BUTTON = (xpath, "//input[@id='continue']")
    FINISH_BUTTON = (xpath, "//button[@id='finish']")
    BACK_HOME_BUTTON = (xpath, "//button[@id='back-to-products']")

    ERROR_MESSAGE = (xpath, "//div[@class='error-message-container error']")
