from .base_selectors import xpath


class CartPageLocators:
    
    CHECKOUT_BUTTON = (xpath, "//button[@id='checkout']")
    REMOVE_BUTTON = (xpath, "//button[@class='btn btn_secondary btn_small cart_button']")
    BACK_TO_INVENTORY = (xpath, "//button[@id='continue-shopping']")
