import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.options import Options as FirefoxOptions


def pytest_addoption(parser):
    # parser.addoption("--browser", action="store", default="chrome", help="Choose browser: chrome, firefox")
    parser.addoption("--headless", action="store", default="yes", help="Choose headless option: yes/no")
    parser.addoption("--url", action="store", default="https://www.saucedemo.com", help="enter your url")


@pytest.fixture
def url(request):
    return request.config.getoption("--url")


@pytest.fixture(params=[
    "chrome",
    "firefox"
])
def browser(request, url):
    browser_name = request.param
    # browser_name = request.config.getoption("browser")
    headless = request.config.getoption("headless")
    print(f"\n\nStart {browser_name} browser for testing...")

    if browser_name == "chrome":
        options = ChromeOptions()
        options.add_argument("--start-maximized")  # запуск браузера "во весь экран"
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--disable-gpu")
        if headless == "yes":
            options.add_argument("--headless")  # безголовый запуск браузера

        browser = webdriver.Remote(command_executor="http://selenium__standalone-chrome:4444/wd/hub", options=options)

    elif browser_name == "firefox":
        options = FirefoxOptions()
        options.add_argument("--start-maximized")  # запуск браузера "во весь экран"
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--disable-gpu")
        if headless == "yes":
            options.add_argument("--headless")  # безголовый запуск браузера

        browser = webdriver.Remote(command_executor="http://selenium__standalone-firefox:4444/wd/hub", options=options)

    else:
        raise pytest.UsageError("--browser should be chrome or firefox")

    def open_browser():
        return browser.get(url)

    browser.open = open_browser
    browser.open()

    browser.implicitly_wait(5)

    yield browser

    print(f"\nQuit {browser_name} browser...")
    browser.quit()
