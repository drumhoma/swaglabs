import allure
import pytest

from .pages.cart_page import CartPage
from .pages.checkout_page import CheckOutPage
from .pages.inventory_page import InventoryPage
from .pages.login_page import LoginPage
from .pages.main_page import MainPage


@allure.feature('All users can add to basket')
@allure.story('Действия с корзиной')
@pytest.mark.parametrize("username", [
    "standard_user",
    "problem_user",
    "performance_glitch_user"
])
@pytest.mark.parametrize("password", [
    "secret_sauce"
])
@pytest.mark.user_add_to_basket
class TestUserAddToBasket:

    @pytest.fixture(scope="function", autouse=True)
    def setup(self, browser, username, password):
        LoginPage(browser) \
            .enter_login(username) \
            .enter_password(password) \
            .login_button_click() \
            .should_be_successful_login()

    def test_user_can_open_item_card(self, browser):
        InventoryPage(browser) \
            .should_be_correct_name_in_card() \
            .should_be_back_to_inventory_page() \
            .should_be_correct_price_in_basket()

    def test_user_can_select_name_product_sort(self, browser):
        InventoryPage(browser) \
            .product_sort(1)  # индекс списка сортировки 0 - 3

    def test_user_can_select_price_product_sort(self, browser):
        InventoryPage(browser) \
            .product_sort(3)  # индекс списка сортировки 0 - 3

    def test_user_can_add_to_basket(self, browser):
        InventoryPage(browser) \
            .should_be_add_to_basket_button() \
            .add_to_basket() \
            .should_be_number_in_basket()
        MainPage(browser) \
            .basket_button_click()

    def test_user_can_remove_from_page(self, browser):
        InventoryPage(browser) \
            .should_be_add_to_basket_button() \
            .add_to_basket() \
            .should_be_number_in_basket() \
            .remove_from_page() \
            .should_not_be_number_in_basket()
        MainPage(browser) \
            .basket_button_click()

    def test_user_can_remove_from_basket(self, browser):
        InventoryPage(browser) \
            .should_be_add_to_basket_button() \
            .add_to_basket() \
            .should_be_number_in_basket()
        MainPage(browser) \
            .basket_button_click()
        CartPage(browser) \
            .remove_from_basket() \
            .go_to_inventory()
        InventoryPage(browser) \
            .should_not_be_number_in_basket()

    @allure.story('Критический путь')
    def test_critical_path_from_add_to_buy(self, browser):
        InventoryPage(browser) \
            .should_be_add_to_basket_button() \
            .product_sort(2) \
            .add_to_basket() \
            .product_sort(3) \
            .add_to_basket() \
            .should_be_number_in_basket()
        MainPage(browser) \
            .basket_button_click()
        CartPage(browser) \
            .remove_from_basket() \
            .checkout_button_click()
        CheckOutPage(browser) \
            .checkout_first_name("Test") \
            .checkout_last_name("Test") \
            .checkout_zip_code(123) \
            .continue_button_click() \
            .should_be_successful_checkout() \
            .finish_button_click() \
            .back_home_button_click()

    @allure.story('Данные пользователя (позитив)')
    @pytest.mark.positive
    @pytest.mark.parametrize("checkout_information", [
        ("Test", "Test", "Test"),
        (123, 123, 123)
    ])
    def test_checkout_information_positive(self, browser, checkout_information):
        MainPage(browser) \
            .basket_button_click()
        CartPage(browser) \
            .checkout_button_click()
        CheckOutPage(browser) \
            .checkout_first_name(checkout_information[0]) \
            .checkout_last_name(checkout_information[1]) \
            .checkout_zip_code(checkout_information[2]) \
            .continue_button_click() \
            .should_be_successful_checkout() \
            .finish_button_click() \
            .back_home_button_click()

    @allure.story('Данные пользователя (негатив)')
    @pytest.mark.negative
    @pytest.mark.parametrize("checkout_information", [
        ("Test", 123, ""),
        (" ", " ", " ")
    ])
    @pytest.mark.xfail
    def test_checkout_information_negative(self, browser, checkout_information):
        MainPage(browser) \
            .basket_button_click()
        CartPage(browser) \
            .checkout_button_click()
        CheckOutPage(browser) \
            .checkout_first_name(checkout_information[0]) \
            .checkout_last_name(checkout_information[1]) \
            .checkout_zip_code(checkout_information[2]) \
            .continue_button_click() \
            .should_be_successful_checkout() \
            .finish_button_click() \
            .back_home_button_click()
