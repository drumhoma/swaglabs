import allure
import pytest

from .pages.main_page import MainPage
from .pages.login_page import LoginPage


@allure.feature('All users can click social links')
@allure.story('Социальные сети')
@pytest.mark.parametrize("username", [
    "standard_user",
    "problem_user",
    "performance_glitch_user"
])
@pytest.mark.parametrize("password", [
    "secret_sauce"
])
@pytest.mark.user_can_click_social_links
class TestLinks:

    @pytest.fixture(scope="function", autouse=True)
    def setup(self, browser, username, password):
        LoginPage(browser) \
            .enter_login(username) \
            .enter_password(password) \
            .login_button_click() \
            .should_be_successful_login()

    def test_user_can_open_menu(self, browser):
        MainPage(browser) \
            .open_menu() \
            .close_menu()

    def test_user_can_open_twitter_link(self, browser):
        MainPage(browser) \
            .should_be_twitter_link() \
            .twitter_link_click()

    def test_user_can_open_fb_link(self, browser):
        MainPage(browser) \
            .should_be_fb_link() \
            .fb_link_click()

    def test_user_can_open_linkedin_link(self, browser):
        MainPage(browser) \
            .should_be_linkedin_link() \
            .linkedin_link_click()
