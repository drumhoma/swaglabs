import allure
import pytest

from .pages.login_page import LoginPage
from .pages.main_page import MainPage



@allure.feature('All users can login')
@pytest.mark.users_can_login
class TestLogin:

    @allure.story('Атрибуты логина')
    def test_should_be_login_attributes(self, browser):
        LoginPage(browser) \
            .should_be_login_logo() \
            .should_be_login_button()

    @allure.story('Авторизация (позитив)')
    @pytest.mark.positive
    @pytest.mark.parametrize("username", [
        "standard_user",
        "locked_out_user",
        "problem_user",
        "performance_glitch_user"
    ])
    @pytest.mark.parametrize("password", [
        "secret_sauce"
    ])
    def test_all_users_can_login_positive(self, browser, username, password):
        LoginPage(browser) \
            .enter_login(username) \
            .enter_password(password) \
            .login_button_click() \
            .should_be_successful_login()

    @allure.story('Авторизация (негатив)')
    @pytest.mark.negative
    @pytest.mark.xfail
    @pytest.mark.parametrize("password", [
        "no_secret_sauce"
    ])
    @pytest.mark.parametrize("username", [
        "standard_user",
        "unregistered_user"
    ])
    def test_all_users_can_login_negative(self, browser, username, password):
        LoginPage(browser) \
            .enter_login(username) \
            .enter_password(password) \
            .login_button_click() \
            .should_be_successful_login()
