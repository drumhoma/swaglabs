from project_tests_ui.locators import LoginPageLocators

from .base_page import BasePage


class LoginPage(BasePage):

    def should_be_login_logo(self):
        # проверка, что на странице есть логотип
        assert self.element_is_present(*LoginPageLocators.LOGIN_LOGO), "Login logo is not presented!"
        return self

    def should_be_login_button(self):
        # проверка, что на странице есть кнопка для логина
        assert self.element_is_present(*LoginPageLocators.LOGIN_BUTTON), "Login button is not presented!"
        return self

    def enter_login(self, username):
        self.do_input(*LoginPageLocators.USERNAME_FIELD, username)
        return self

    def enter_password(self, password):
        self.do_input(*LoginPageLocators.PASSWORD_FIELD, password)
        return self

    def login_button_click(self):
        self.do_click(*LoginPageLocators.LOGIN_BUTTON)
        return self

    def should_be_successful_login(self):
        # проверка, что на странице нет сообщения об ошибке авторизации
        assert self.element_is_not_present(*LoginPageLocators.ERROR_MESSAGE), "Authorization ERROR!"
        return self
