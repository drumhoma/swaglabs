from project_tests_ui.locators import MainPageLocators

from .base_page import BasePage


class MainPage(BasePage):

    def should_be_app_logo(self):
        # проверка, что на странице есть логотип
        assert self.element_is_present(*MainPageLocators.APP_LOGO), "App logo is not presented!"
        return self

    def open_menu(self):
        # открыть сайд меню
        self.do_click(*MainPageLocators.SIDE_MENU)
        return self

    def close_menu(self):
        # закрыть сайд меню
        self.wait_for_visible(*MainPageLocators.CLOSE_MENU)
        self.do_click(*MainPageLocators.CLOSE_MENU)
        return self

    def basket_button_click(self):
        # переход в корзину
        self.do_click(*MainPageLocators.SHOP_CART)
        return self

    def should_be_twitter_link(self):
        # проверка, что на странице есть логотип
        assert self.element_is_present(*MainPageLocators.TWITTER_LINK), "Twitter link is not presented"
        return self

    def should_be_fb_link(self):
        # проверка, что на странице есть кнопка для логина
        assert self.element_is_present(*MainPageLocators.FB_LINK), "FB link is not presented"
        return self

    def should_be_linkedin_link(self):
        # проверка, что на странице есть логотип
        assert self.element_is_present(*MainPageLocators.LINKEDIN_LINK), "Linkedin_link is not presented"
        return self

    def twitter_link_click(self):
        self.do_click(*MainPageLocators.TWITTER_LINK)

    def fb_link_click(self):
        self.do_click(*MainPageLocators.FB_LINK)

    def linkedin_link_click(self):
        self.do_click(*MainPageLocators.LINKEDIN_LINK)
