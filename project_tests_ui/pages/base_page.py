import time

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class BasePage:

    def __init__(self, browser):
        self.browser = browser

    def get_element(self, how, what):
        # получение элемента
        return self.browser.find_element(how, what)

    def get_elements(self, how, what):
        # получение списка элементов
        return self.browser.find_elements(how, what)

    def do_click(self, how, what):
        # return ActionChains(self.browser).move_to_element(self.get_element(how, what)).click().perform()
        self.get_element(how, what).click()
        return self

    def clear_input(self, how, what):
        element = self.get_element(how, what)
        element.send_keys(Keys.CONTROL, "a")
        element.send_keys(Keys.BACK_SPACE)
        return self

    def do_input(self, how, what, value):
        element = self.get_element(how, what)
        element.clear()
        element.send_keys(value)
        return self

    def get_text_element(self, how, what):
        # получение текста из атрибута локатора
        return self.get_element(how, what).text

    def wait_for_visible(self, how, what, timeout=5):
        return WebDriverWait(self.browser, timeout).until(EC.visibility_of(self.get_element(how, what)))

    def wait_for_clickable(self, how, what, timeout=5):
        return WebDriverWait(self.browser, timeout).until(EC.element_to_be_clickable(self.get_element(how, what)))

    def element_is_present(self, how, what):
        # проверка, что элемент присутствует на странице
        try:
            self.get_element(how, what)
        except NoSuchElementException:
            return False
        return True

    def element_is_not_present(self, how, what, timeout=5):
        # проверка, что элемент не появляется на странице в течение заданного времени
        try:
            WebDriverWait(self.browser, timeout).until(EC.presence_of_element_located((how, what)))
        except TimeoutException:
            return True
        return False

    def element_is_disappeared(self, how, what, timeout=5):
        # проверка, что элемент исчезает со страницы в течение заданного времени
        try:
            WebDriverWait(self.browser, timeout, 1, TimeoutException).until_not(
                EC.presence_of_element_located((how, what)))
        except TimeoutException:
            return False
        return True

    def do_screenshot(self):
        self.browser.save_screenshot(f'./screenshots/{time.time()}_screenshot.png')
        return self
