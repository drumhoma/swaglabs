from project_tests_ui.locators import CartPageLocators

from .base_page import BasePage


class CartPage(BasePage):

    def checkout_button_click(self):
        self.do_click(*CartPageLocators.CHECKOUT_BUTTON)
        return self

    def go_to_inventory(self):
        self.do_click(*CartPageLocators.BACK_TO_INVENTORY)
        return self

    def remove_from_basket(self):
        self.do_click(*CartPageLocators.REMOVE_BUTTON)
        return self

    def remove_all_from_basket(self):
        for i in self.get_elements(*CartPageLocators.REMOVE_BUTTON):
            i.do_click()
        return self
