from .base_page import BasePage
from .cart_page import CartPage
from .checkout_page import CheckOutPage
from .inventory_page import InventoryPage
from .login_page import LoginPage
from .main_page import MainPage

