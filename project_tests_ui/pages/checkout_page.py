from project_tests_ui.locators import CheckOutPageLocators

from .base_page import BasePage


class CheckOutPage(BasePage):

    def should_be_login_url(self):
        # проверка, что открыта страница регистрации/авторизации
        url = self.browser.current_url
        assert "login" in url, "'Login' is not contain in URL!"
        return self

    def checkout_first_name(self, first_name):
        self.do_input(*CheckOutPageLocators.FIRST_NAME, first_name)
        return self

    def checkout_last_name(self, last_name):
        self.do_input(*CheckOutPageLocators.LAST_NAME, last_name)
        return self

    def checkout_zip_code(self, zip_code):
        self.do_input(*CheckOutPageLocators.ZIP_CODE, zip_code)
        return self

    def should_be_successful_checkout(self):
        # проверка, что нет сообщения об ошибке - все поля заполнены
        assert self.element_is_not_present(*CheckOutPageLocators.ERROR_MESSAGE), "Checkout ERROR!"
        return self

    def continue_button_click(self):
        self.do_click(*CheckOutPageLocators.CONTINUE_BUTTON)
        return self

    def finish_button_click(self):
        self.wait_for_visible(*CheckOutPageLocators.FINISH_BUTTON)
        self.do_click(*CheckOutPageLocators.FINISH_BUTTON)
        return self

    def back_home_button_click(self):
        self.wait_for_visible(*CheckOutPageLocators.BACK_HOME_BUTTON)
        self.do_click(*CheckOutPageLocators.BACK_HOME_BUTTON)
        return self
