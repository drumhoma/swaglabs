from selenium.webdriver.support.ui import Select

from project_tests_ui.locators import InventoryPageLocators
from .base_page import BasePage


class InventoryPage(BasePage):

    def should_be_add_to_basket_button(self):
        # проверка, что кнопка добавить в корзину есть на странице
        assert self.element_is_present(*InventoryPageLocators.ADD_TO_CART), "'Add to basket' button was not found!"
        return self

    def add_to_basket(self):
        # добавление товара в корзину
        self.do_click(*InventoryPageLocators.ADD_TO_CART)
        return self

    def add_all_to_basket(self):
        # добавление всех товаров в корзину
        for i in self.get_elements(*InventoryPageLocators.ADD_TO_CART):
            i.do_click()
        return self

    def remove_from_page(self):
        # удаление одного товара из корзины с главной страницы
        self.do_click(*InventoryPageLocators.REMOVE_FROM_CART)
        return self

    def remove_all_from_page(self):
        # удаление всех товаров из корзины с главной страницы
        for i in self.get_elements(*InventoryPageLocators.REMOVE_FROM_CART):
            i.do_click()
        return self

    def should_be_number_in_basket(self):
        # проверка, что на корзине появился индикатор количества товара
        assert self.element_is_present(*InventoryPageLocators.NUM_IN_CART), "Item was not added to basket!"
        return self

    def should_not_be_number_in_basket(self):
        # проверка, что на корзине отсутствует индикатор количества товара
        assert self.element_is_not_present(*InventoryPageLocators.NUM_IN_CART), "Item was not deleted from basket!"
        return self

    def should_be_back_to_inventory_page(self):
        self.do_click(*InventoryPageLocators.BACK_TO_INVENTORY)
        return self

    def should_be_correct_name_in_card(self):
        # проверка, что наименование товара в корзине соответствует наименованию на странице товара
        name = self.get_text_element(*InventoryPageLocators.ITEM_NAME_IN_LINK)
        self.do_click(*InventoryPageLocators.ITEM_NAME_IN_LINK)
        card_name = self.get_text_element(*InventoryPageLocators.ITEM_NAME_IN_CARD)
        assert name == card_name, "The item name in the card is different!"
        return self

    def should_be_correct_price_in_basket(self):
        # проверка, что цена товара в корзине соответствует цене на странице товара
        price = self.get_text_element(*InventoryPageLocators.ITEM_PRICE_IN_LINK)
        self.do_click(*InventoryPageLocators.ITEM_NAME_IN_LINK)
        card_price = self.get_text_element(*InventoryPageLocators.ITEM_PRICE_IN_CARD)
        assert price == card_price, "The item price in the card is different!"
        return self

    def product_sort(self, sort):
        # выбор сортировки товара - тест не работает
        select_menu = Select(self.get_element(*InventoryPageLocators.SELECT_MENU))
        value_select = []
        for i in self.get_elements(*InventoryPageLocators.SELECT_VALUE):
            value_select.append(i.text)
        select_menu.select_by_visible_text(value_select[sort])
        assert value_select[0] != value_select[sort], "Items was not sorted!"
        return self
