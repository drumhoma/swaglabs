## test_repository_auto_testing includes:
#### 1. project_code - fake app code
#### 2. project_test_ui:
##### 2.1. python+pytest+selenium ui tests, running with chrome and firefox
##### 2.2. fake api tests - link from postman collection, running with newman
#### 3. gitlab-ci config - runs all tests after push in master branch
#### 4. tests runs in docker containers